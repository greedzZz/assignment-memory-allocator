#define _DEFAULT_SOURCE

#include <unistd.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static uint8_t count_blocks(struct block_header* block) {
    uint8_t count = 0;
    while (block) {
        if (!block->is_free) count++;
        block = block->next;
   }
   return count;
}

//Обычное успешное выделение памяти.
static void test1(struct block_header* block) {
    printf("Тест 1: старт.\n");
    void* mem = _malloc(100);
    if(!mem) err("Тест 1: указатель равен NULL.\n");
    if (block->capacity.bytes != 100) err("Тест 1: некорректный размер блока.\n");
    struct block_header* block_header = block_get_header(mem);
    if (block_header->is_free) err("Тест 1: выделенный блок свободен.\n");
    _free(mem);
    printf("Тест 1: завершен.\n");
}

//Освобождение одного блока из нескольких выделенных.
static void test2(struct block_header* block) {
    printf("Тест 2: старт.\n");
    void* mem1 = _malloc(555);
    void* mem2 = _malloc(777);
    if (count_blocks(block) != 2) {
        _free(mem1);
  	    _free(mem2);
        err("Тест 2: выделено неверное количество блоков.\n");
    }
    _free(mem2);
    if (count_blocks(block) != 1) {
        err("Тест 2: не удалось корректно освободить память.\n");
    }
    _free(mem1);
    printf("Тест 2: завершен.\n");
}

//Освобождение двух блоков из нескольких выделенных.
static void test3(struct block_header* block) {
    printf("Тест 3: старт.\n");
    void* mem1 = _malloc(555);
    void* mem2 = _malloc(666);
    void* mem3 = _malloc(777);
    if (count_blocks(block) != 3) {
        _free(mem1);
  	    _free(mem2);
        _free(mem3);
        err("Тест 3: выделено неверное количество блоков.\n");
    }
    _free(mem2);
    _free(mem3);
    if (count_blocks(block) != 1) {
        err("Тест 3: не удалось корректно освободить память.\n");
    }
    _free(mem1);
    printf("Тест 3: завершен.\n");
}

//Память закончилась, новый регион памяти расширяет старый.
static void test4(struct block_header* block) {
    printf("Тест 4: старт.\n");
    void* mem1 = _malloc(8192);
    void* mem2 = _malloc(4096);
    if (count_blocks(block) != 2) {
        _free(mem1);
  	    _free(mem2);
        err("Тест 4: выделено неверное количество блоков.\n");
    }
    struct block_header* header1 = block_get_header(mem1);
    struct block_header* header2 = block_get_header(mem2);
    if ((void*) (header1->contents + header1->capacity.bytes) != (void*) header2) {
        _free(mem1);
  	    _free(mem2);
        err("Тест 4: новый регион памяти расширяет старый.\n");
    }
    _free(mem1);
    _free(mem2);
    printf("Тест 4: завершен.\n");
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
static void test5(struct block_header* block) {
    printf("Тест 5: старт.\n");
    void* mem1 = _malloc(8192);
    while (block->next) block = block->next;
    void* addr = (uint8_t*) block + size_from_capacity(block->capacity).bytes;
    mmap((uint8_t*) (getpagesize() * ((size_t) addr / getpagesize() + (((size_t) addr % getpagesize()) > 0))), 8192,
          PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0);
    void* mem2 = _malloc(32768);
    struct block_header* header = block_get_header(mem2);
    if (block == header) err("Тест 5: новый регион памяти был выделен сразу за старым.\n");
    _free(mem1);
    _free(mem2);
    printf("Тест 5: завершен.\n");
}

static void testing() {
    void* heap = heap_init(8192);
    struct block_header* block = (struct block_header*) heap;
    if (!heap || !block) {
        err("Не удалось начать тестирование.\n");
    } else {
        printf("Тестирование началось.\n");
        test1(block);
        test2(block);
        test3(block);
        test4(block);
        test5(block);
        printf("Тестирование завершилось.\n");
    }
}

int main() {
    testing();
    return 0;
}
